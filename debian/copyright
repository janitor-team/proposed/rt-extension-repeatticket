Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/RT-Extension-RepeatTicket
Upstream-Contact: Best Practical Solutions, LLC <modules@bestpractical.com>
Upstream-Name: RT-Extension-RepeatTicket

Files: *
Copyright: 2013-2017, Best Practical Solutions, LLC <modules@bestpractical.com>
License: GPL-2

Files: debian/*
Copyright: 2016, Joost van Baal-Ilić <joostvb@debian.org>,
           2016, Maarten Horden <m.horden@parsus.nl>,
           2018, Dominic Hargreaves <dom@earth.li>,
           2020, Andrew Ruthven <andrew@etc.gen.nz>,
License: GPL-2+

Files: inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/YAML/Tiny.pm
Copyright: 2006-2013, Adam Kennedy <adamk@cpan.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
